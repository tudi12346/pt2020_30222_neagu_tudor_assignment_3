package com.assignment3.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

import com.assignment3.model.Product;
import com.assignment3.service.ProductService;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Class for displaying the product report. Contains only one static method to
 * create the report
 */
public class ProductReport {
	/**
	 * Static method to create the report.
	 */
	public static void createReport() throws FileNotFoundException, DocumentException, SQLException {
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("Reports_products.pdf"));
		document.open();
		PdfPTable table = new PdfPTable(4);
		PdfPCell cell;
		Stream.of("id", "name", "price", "quantity").forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
		ProductService productservice = new ProductService();
		ArrayList<Product> list = new ArrayList<Product>();
		list = (ArrayList<Product>) productservice.findAll();

		for (Product i : list) {

			cell = new PdfPCell(new Phrase(i.getId().toString()));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(i.getName()));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(String.valueOf(i.getPrice())));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(String.valueOf(i.getQuantity())));
			table.addCell(cell);

		}

		document.add(table);
		document.close();
	}
}