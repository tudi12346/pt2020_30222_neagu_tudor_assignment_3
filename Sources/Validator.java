package com.assignment3.validator;


public interface Validator<T> {
	public void validate(T t);
}

