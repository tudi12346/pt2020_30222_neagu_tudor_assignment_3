package com.assignment3.model;
/** Represents a bill
 */
public class Bill {
	/** Attributes product
     *  id The bill�s id.
     *  clientName The client's name.
     *  productName The product�s name.
     *  productPrice The product�s price.
     *  productQuantity The product�s quantity.
     *  totalPrice The bill's total value(productQuantity*productPrice)
     */
	private Long id;
    private String clientName;
    private String productName;
    private Double productQuantity;
    private Double productPrice;
    private Double totalPrice;
    
    /** Creates a product
     * @param id The bill�s id.
     * @param clientName The client's name.
     * @param productName The product�s name.
     * @param productPrice The product�s price.
     * @param productQuantity The product�s quantity.
     * @param totalPrice The bill's total value(productQuantity*productPrice)
     */
	public Bill(Long id, String clientName, String productName, Double productQuantity, Double productPrice) {
		super();
		this.id = id;
		this.clientName = clientName;
		this.productName = productName;
		this.productQuantity = productQuantity;
		this.productPrice = productPrice;
		setTotalPrice();
	}
	public Bill() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**Sets the id
     * @param long id representing product id
     */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return clientName
	 */
	public String getClientName() {
		return clientName;
	}
	/**Sets the client name
     * @param String clientName representing client name
     */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	/**
	 * @return productName
	 */
	public String getProductName() {
		return productName;
	}
	/**Sets the product name
     * @param String productName representing product name
     */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return productQuantity
	 */
	public Double getProductQuantity() {
		return productQuantity;
	}
	/**Sets the product quantity
     * @param Double productQuantity representing product quantity
     */
	public void setProductQuantity(Double productQuantity) {
		this.productQuantity = productQuantity;
	}
	/**
	 * @return productPrice
	 */
	public Double getProductPrice() {
		return productPrice;
	}
	/**
     * @param Double productPrice representing product price
     */
	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}
	/**
	 * @return totalPrice
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}
	/**
     * @param Double totalPrice representing total price of the bill
     */
	public void setTotalPrice() {
		this.totalPrice = productQuantity*productPrice;
	}
	
}
