package com.assignment3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Scanner;

import com.itextpdf.text.DocumentException;

public class App {

	public static void main(String[] args)  {
		File readFile=new File(args[0]);
		Scanner scanner = null;
		try {
			scanner = new Scanner(readFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			//fp.readFromFile(scanner, "in.txt");
			try {
				ParsingClass.readFromFile(scanner, args[0]);
			} catch (SQLException | DocumentException | URISyntaxException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

}
