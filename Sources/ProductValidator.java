package com.assignment3.validator;

import com.assignment3.model.Product;

/**
 * Represents a product validator;
 * 
 * implements Validator interface with the validate method for verifying the
 * consistency of the data
 */
public class ProductValidator implements Validator<Product> {
	/**
	 * Validates the product given as Product object
	 */
	public void validate(Product product) {
		if (product.getName() == null)
			throw new IllegalArgumentException("Product invalid name!");
		if (product.getPrice() < 0 || product.getQuantity() < 0)
			throw new IllegalArgumentException("Price or quantity is negative!");
	}
}