package com.assignment3.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.assignment3.model.Product;

/**
 * 
 * Repository class used to connect and execute operations on the database
 * 
 * @author Tudor Neagu
 */
public class ProductRepository {
	/**
	 * Attributes are a Logger and the Query strings
	 */
	protected static final Logger LOGGER = Logger.getLogger(ProductRepository.class.getName());
	private final static String updateQuantityWhereId = ("UPDATE product SET quantity=quantity-? where id=?;");
	private final static String getQuantity = ("SELECT quantity FROM product where id=?");
	private static final String findByName = "SELECT id from product where name=? and quantity > 0";
	private final static String findById = "SELECT * FROM product where id = ?";
	private final static String delete = "DELETE FROM product WHERE name=?";
	private final static String findAll = "SELECT * FROM product";
	private static final String insert = "INSERT INTO product (name,price,quantity)" + " VALUES (?,?,?)";

	/**
	 * Updates the quantity of the specified product;subtracts the specified
	 * quantity from the quantity stored in the database
	 * 
	 * @param quantity -> the quantity to subtract
	 * @param id       -> the id of the product
	 * @return true if successful, false otherwise
	 * @throws SQLException
	 */
	public static boolean updateQuantityWhereId(Double quantity, Long id) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement(updateQuantityWhereId);
			statement.setDouble(1, quantity);
			statement.setLong(2, id);
			statement.executeUpdate();

			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductRepo: updateQuantityWhereId " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return false;
	}

	/**
	 * Gets the existing quantity of the specified product
	 * 
	 * @param id representing the id of the product
	 * @return quantity Double
	 * @throws SQLException
	 */
	public static Double getQuantity(Long id) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Double quantity = 0D;
		try {
			statement = connection.prepareStatement(getQuantity);
			statement.setLong(1, id);// primul parametru primeste clientId
			rs = statement.executeQuery();// si se cauta clientii cu id-ul respectiv
			rs.next();
			quantity = rs.getDouble("quantity");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductRepo: getQuantity " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return quantity;
	}

	/**
	 * Finds product by name
	 * 
	 * @param name
	 * @return the id of the product or -1 if product name is not in the database
	 * 
	 */
	public static Long findByName(String name) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Long id = -1L;
		try {
			statement = connection.prepareStatement(findByName);
			statement.setString(1, name);// primul parametru primeste clientId
			rs = statement.executeQuery();// si se cauta clientii cu id-ul respectiv
			if (rs.next())
				id = rs.getLong("id");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductRepo: findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return id;
	}

	/**
	 * Finds product by id
	 * 
	 * @param id
	 * @return object representation of the product with the specified id, null if
	 *         not found
	 * 
	 */
	public static Product findById(Long id) throws SQLException {
		Product product = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement(findById);
			statement.setLong(1, id);// primul parametru primeste clientId
			rs = statement.executeQuery();// si se cauta clientii cu id-ul respectiv
			rs.next();
			String name = rs.getString("name");
			Double price = rs.getDouble("price");
			Double quantity = rs.getDouble("quantity");
			product = new Product(id, name, price, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductRepo: findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return product;
	}

	/**
	 * Deletes product from the database product table
	 * 
	 * @param name-> String representing the name of the product to be deleted
	 * @return true if successful, false otherwise
	 */
	public static boolean deleteProduct(String name) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement(delete, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, name);
			statement.executeUpdate();
			rs = statement.getGeneratedKeys();
			rs.next();
			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductRepo: delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return false;

	}

	/**
	 * Retrieves all the products from the product table
	 * 
	 * @return ArrayList<Product> list of product objects created form the existing
	 *         products in the database's product table
	 */
	public static List<Product> findAll() throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<Product> products = new ArrayList<Product>();

		try {
			statement = connection.prepareStatement(findAll);
			rs = statement.executeQuery();
			while (rs.next()) {
				Product p = new Product();
				p.setId(rs.getLong("id"));
				p.setName(rs.getString("name"));
				p.setQuantity(rs.getDouble("quantity"));
				p.setPrice(rs.getDouble("price"));
				products.add(p);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductRepo: findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return products;
	}

	/**
	 * Inserts product object into the database product table
	 * 
	 * @param product
	 * @return true if the product was inserted successfully; false otherwise
	 */
	public static Long insert(Product product) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		Long insertedId = -1L;
		try {
			insertStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getName());
			insertStatement.setDouble(2, product.getPrice());
			insertStatement.setDouble(3, product.getQuantity());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getLong(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product repo: insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}

}
