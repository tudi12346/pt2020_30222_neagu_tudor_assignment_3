package com.assignment3.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.assignment3.model.Client;

/**
 * Repository class used to connect and execute operations on the database
 * 
 * @author Tudor Neagu
 */
public class ClientRepository {
	/**
	 * Attributes are a Logger and the Query strings
	 */
	protected static final Logger LOGGER = Logger.getLogger(ClientRepository.class.getName());
	private static final String findByName = "SELECT id FROM client where name= ?";
	private final static String findById = "SELECT * FROM client where id = ?";
	private final static String delete = "DELETE FROM client WHERE name= ?";
	private static final String insert = "INSERT INTO client (name,address)" + " VALUES (?,?)";

	/**
	 * Finds client by name
	 * 
	 * @param name
	 * @return the id of the client or -1 if client name is not in the database
	 * 
	 */
	public static Long findByName(String name) {
		Long id = -1L;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement(findByName);
			statement.setString(1, name);
			rs = statement.executeQuery();
			if (rs.next())
				id = rs.getLong("id");

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client repo :findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return id;
	}

	/**
	 * Finds client by id
	 * 
	 * @param id
	 * @return object representation of the client with the specified id, null if
	 *         not found
	 * 
	 */
	public static Client findById(Long id) {
		Client result = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement(findById);
			statement.setLong(1, id);
			rs = statement.executeQuery();
			rs.next();
			String name = rs.getString("name");
			String adress = rs.getString("adress");
			result = new Client(id, name, adress);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client repo: findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return result;
	}

	/**
	 * Deletes client form client table in the database
	 * 
	 * @param client -> as a Client object
	 * @return true if successful, false otherwise
	 */
	public static boolean deleteClient(Client client) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement(delete, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, client.getName());
			statement.executeUpdate();
			rs = statement.getGeneratedKeys();
			rs.next();
			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client repo: delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return false;

	}

	/**
	 * Deletes all the clients in the client table
	 * 
	 * @return true if successful, false otherwise
	 */
	public static boolean deleteAll() {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.prepareStatement("DELETE FROM client", Statement.RETURN_GENERATED_KEYS);
			statement.executeUpdate();
			rs = statement.getGeneratedKeys();
			rs.next();
			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client repo: deleteAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);

		}
		return false;
	}

	/**
	 * Inserts client object into the database client table
	 * 
	 * @param client
	 * @return true if the client was inserted successfully; false otherwise
	 */
	public static Long insert(Client client) {
		Connection connection = ConnectionFactory.getConnection();

		PreparedStatement statement = null;
		Long generatedId = -1L;

		try {
			statement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, client.getName());
			statement.setString(2, client.getAddress());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				generatedId = rs.getLong(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client repo:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return generatedId;
	}

}