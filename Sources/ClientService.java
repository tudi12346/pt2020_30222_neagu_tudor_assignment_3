package com.assignment3.service;

import java.util.ArrayList;
import java.util.List;

import com.assignment3.model.Client;
import com.assignment3.repository.ClientRepository;
import com.assignment3.validator.ClientValidator;
import com.assignment3.validator.Validator;

/**
 * 
 * Service class used to execute operations with Client objects
 * 
 * @author Tudor Neagu
 */
public class ClientService {
	/**
	 * A list of validators to validate the data before using it
	 */
	private List<Validator<Client>> validators;

	/**
	 * initializes the validator list
	 */
	public ClientService() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new ClientValidator());

	}

	/**
	 * Finds client by name
	 * 
	 * @param name
	 * @return the id of the client or -1 if client name is not in the database
	 * 
	 */
	public Long findByName(String name) {
		return ClientRepository.findByName(name);
	}

	/**
	 * Finds client by id
	 * 
	 * @param id
	 * @return object representation of the client with the specified id, null if
	 *         not found
	 * 
	 */
	public Client findById(Long id) {
		return ClientRepository.findById(id);
	}

	/**
	 * Deletes client form client table in the database
	 * 
	 * @param client -> as a Client object
	 * @return true if successful, false otherwise
	 */
	public boolean deleteClient(Client client) {
		return ClientRepository.deleteClient(client);

	}

	/**
	 * Deletes all the clients in the client table
	 * 
	 * @return true if successful, false otherwise
	 */
	public boolean deleteAll() {
		return ClientRepository.deleteAll();
	}

	/**
	 * Inserts client object into the database client table
	 * 
	 * @param client
	 * @return true if the client was inserted successfully; false otherwise
	 */
	public Long insert(Client client) {
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientRepository.insert(client);
	}

}
