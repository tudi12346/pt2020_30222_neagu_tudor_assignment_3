package com.assignment3.model;
/** Represents a product
 */
public class Product {
	
	/** Attributes product
     *  id The productís id.
     *  name The productís name.
     *  price The productís price.
     *  quantity The productís quantity.
     */
	private Long id;
	private String name;
	private Double price;
	private Double quantity;

	/** Creates a product
     * @param id The productís id.
     * @param name The productís name.
     * @param quantity The productís quantity.
     * @param price The productís price.
     */
	public Product(Long id,String name, Double price, Double quantity) {
		
		this.setId(id);
		this.setName(name);
		this.setPrice(price);
		this.setQuantity(quantity);
	}
	/** Creates a product
     * @param name The productís name.
     * @param quantity The productís quantity.
     * @param price The productís price.
     */
	public Product(String name, Double quantity, Double price) {
		
		this.setName(name);
		this.setPrice(price);
		this.setQuantity(quantity);
	}
	/** Creates a product
	*/
	public Product() {
	}
	/**
	 * @return quantity
	 */
	public Double getQuantity() {
		return quantity;
	}

	 /**
     * @param quantity double representing product quantity
     */
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return price
	 */
	public Double getPrice() {
		return price;
	}

	 /**
     * @param price double representing product price
     */
	public void setPrice(Double price) {
		this.price = price;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
     * @param string name representing product name
     */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
     * @param long id representing product id
     */
	public void setId(Long id) {
		this.id = id;
	}

}
