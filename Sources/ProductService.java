package com.assignment3.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.assignment3.model.Product;
import com.assignment3.repository.ProductRepository;
import com.assignment3.validator.ProductValidator;
import com.assignment3.validator.Validator;

/**
 * 
 * Service class used to execute operations with Product objects
 * 
 * @author Tudor Neagu
 */
public class ProductService {
	/**
	 * A list of validators to validate the data before using it
	 */
	private List<Validator<Product>> validators;

	/**
	 * initializes the validator list
	 */
	public ProductService() {
		validators = new ArrayList<Validator<Product>>();
		validators.add(new ProductValidator());

	}

	/**
	 * Finds product by id
	 * 
	 * @param id
	 * @return object representation of the product with the specified id, null if
	 *         not found
	 * 
	 */
	public Product findById(Long id) throws SQLException {
		Product p = ProductRepository.findById(id);
		if (p == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return p;
	}

	/**
	 * Inserts product object into the database product table
	 * 
	 * @param product
	 * @return true if the product was inserted successfully; false otherwise
	 */
	public Long insert(Product product) {
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
		return ProductRepository.insert(product);
	}

	/**
	 * Deletes product from the database product table
	 * 
	 * @param name-> String representing the name of the product to be deleted
	 * @return true if successful, false otherwise
	 */
	public boolean deleteProduct(String product) {
		ProductRepository.deleteProduct(product);
		return true;
	}

	/**
	 * Retrieves all the products from the product table
	 * 
	 * @return ArrayList<Product> list of product objects created form the existing
	 *         products in the database's product table
	 */
	public List<Product> findAll() throws SQLException {
		List<Product> all = new ArrayList<Product>();
		all = ProductRepository.findAll();
		return all;
	}

	/**
	 * Finds product by name
	 * 
	 * @param name
	 * @return the id of the product or -1 if product name is not in the database
	 * 
	 */
	public Long findByName(String name) {
		try {
			return ProductRepository.findByName(name);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1L;
	}

	/**
	 * Gets the existing quantity of the specified product
	 * 
	 * @param id representing the id of the product
	 * @return quantity Double
	 * @throws SQLException
	 */
	public Double getQuantity(Long id) {
		try {
			return ProductRepository.getQuantity(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Updates the quantity of the specified product;subtracts the specified
	 * quantity from the quantity stored in the database
	 * 
	 * @param quantity -> the quantity to subtract
	 * @param id       -> the id of the product
	 * @return true if successful, false otherwise
	 * @throws SQLException
	 */
	public boolean updateQuantityWhereId(Double q, Long id) {
		try {
			return ProductRepository.updateQuantityWhereId(q, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
