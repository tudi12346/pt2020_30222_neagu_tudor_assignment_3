package com.assignment3.validator;

import com.assignment3.model.Client;

/**
 * Represents a client validator;
 * 
 * implements Validator interface with the validate method for verifying the
 * consistency of the data
 */
public class ClientValidator implements Validator<Client> {
	/**
	 * Validates the client given as Client object
	 */
	public void validate(Client client) {
		if (client.getName() == null || client.getAddress() == null)
			throw new IllegalArgumentException("Invalid client!");
	}
}
