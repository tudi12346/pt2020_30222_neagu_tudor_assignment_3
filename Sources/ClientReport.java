package com.assignment3.report;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Stream;

import com.assignment3.repository.ConnectionFactory;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Class for displaying the client report. Contains only one static method to
 * create the report
 */
public class ClientReport {
	/**
	 * Static method to create the report.
	 */
	public static void createReport() throws URISyntaxException, IOException, DocumentException, SQLException {

		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("Report_clients.pdf"));
		document.open();
		PdfPTable table = new PdfPTable(3);
		PdfPCell cell;
		Stream.of("id", "name", "address").forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		String sql = "SELECT * from client";
		statement = connection.prepareStatement(sql);
		rs = statement.executeQuery();

		while (rs.next()) {
			Long id = rs.getLong("id");
			cell = new PdfPCell(new Phrase(id.toString()));
			table.addCell(cell);

			String name = rs.getString("name");
			cell = new PdfPCell(new Phrase(name));
			table.addCell(cell);

			String address = rs.getString("address");
			cell = new PdfPCell(new Phrase(address));
			table.addCell(cell);

		}

		document.add(table);
		document.close();
	}

}