package com.assignment3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Scanner;

import com.assignment3.model.Bill;
import com.assignment3.model.Client;
import com.assignment3.model.Order;
import com.assignment3.model.Product;
import com.assignment3.report.ClientReport;
import com.assignment3.report.ErrorReport;
import com.assignment3.report.OrderReport;
import com.assignment3.report.ProductReport;
import com.assignment3.repository.BillRepository;
import com.assignment3.repository.OrderRepository;
import com.assignment3.service.ClientService;
import com.assignment3.service.ProductService;
import com.itextpdf.text.DocumentException;
/**
 * Class to read and parse commands from file and execute them 
 * @author Tudor Neagu
 *
 */
public class ParsingClass {
	/**Static Method to begin reading and executing
	 * 
	 * @param scanner
	 * @param fileName
	 * @throws SQLException
	 * @throws DocumentException
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static void readFromFile(Scanner scanner, String fileName)
			throws SQLException, DocumentException, URISyntaxException, IOException {
		String[] line, attributes;
		String productName;
		Client c;
		Product p;
		
		File file = new File(fileName);
		scanner = new Scanner(file);
		while (scanner.hasNextLine()) {
			line = scanner.nextLine().split(":");// se imparte linia la :->line[0] va fi comanda, iar line[1] atributele
													// separate prin virgula

			if (line[0].equals("Insert client")) {
				attributes = line[1].split(",");// se pun atributele in lista de atribute

				ClientService clientService = new ClientService();
				if (clientService.findByName(attributes[0]) == -1L) {
					c = new Client(attributes[0], attributes[1]);// attributes[0] va fi numele, iar attributes[1] adresa
					clientService.insert(c);
				}
			}

			if (line[0].equals("Insert product")) {
				attributes = line[1].split(",");
				ProductService productService = new ProductService();
				Long id = productService.findByName(attributes[0]);
				if (id == -1L) {
					p = new Product(attributes[0], Double.parseDouble(attributes[1]),
							Double.parseDouble(attributes[2]));
					// attributes[0] va fi numele, attr[1] va fi quantity, iar attr[2] va fi price
					// ul
					try {
						productService.insert(p);
					} catch (Exception ex) {
						System.out.println("Could not add the product ");
					}
				} else
					productService.updateQuantityWhereId(-Double.parseDouble(attributes[1]), id);
			}

			if (line[0].equals("Delete client")) {
				attributes = line[1].split(",");

				ClientService clientService = new ClientService();
				Long id = clientService.findByName(attributes[0]);// gasim id ul clientului cu numele respectiv
				c = new Client(id, attributes[0], attributes[1]);
				try {
					clientService.deleteClient(c);
				} catch (Exception ex) {
					System.out.println("Could not delete the client");
				}
			}

			if (line[0].equals("Delete product")) {
				attributes = line[1].split(",");
				productName = attributes[0];
				ProductService productService = new ProductService();
				productService.deleteProduct(productName);
			}

			if (line[0].equals("Order")) {
				attributes = line[1].split(",");
				String clientName;
				String productNamee;
				Double quantity;
				
				clientName = attributes[0];
				productNamee = attributes[1];
				quantity = Double.parseDouble(attributes[2]);
				
				Long clientId = -1L;
				Long productId = -1L;
				ClientService clientService = new ClientService();
				ProductService productService = new ProductService();

				productId = productService.findByName(productNamee);
				if (productService.getQuantity(productId) >= quantity) {//daca se poate genera bill ul
					
					clientId = clientService.findByName(clientName);
					productId = productService.findByName(productNamee);
					productService.updateQuantityWhereId(Double.parseDouble(quantity.toString()),productId);
					Order o = new Order();
					
					o.setClientId(clientId);
					o.setProductId(productId);
					OrderRepository.insert(o);//se creeaza si insereaza order ul 
					o.setId(OrderRepository.getLastId());
					Product prod=new Product();
					prod=productService.findById(productId);
					Bill bill= new Bill(o.getId(), clientName, productNamee, quantity, prod.getPrice());
					BillRepository.insert(bill);//se creeaza si se insereaza bill ul 
				} else
					try {
						ErrorReport.reportError(clientName,productNamee,quantity);
					} catch (FileNotFoundException | DocumentException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}


			if (line[0].equals("Report client")) {
				ClientReport.createReport();
			}
			if (line[0].equals("Report product")) {
				ProductReport.createReport();
			}
			if (line[0].equals("Report order")) {
				OrderReport.createReport();
			}
		}

		scanner.close();
	}
}