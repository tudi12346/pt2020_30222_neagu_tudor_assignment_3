package com.assignment3.report;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

import com.assignment3.model.Bill;
import com.assignment3.repository.BillRepository;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Class for displaying the order report. Contains only one static method to
 * create the report
 */
public class OrderReport {
	/**
	 * Static method to create the report.
	 */
	public static void createReport() throws URISyntaxException, IOException, DocumentException, SQLException {

		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("Report_orders.pdf"));
		document.open();
		PdfPTable table = new PdfPTable(6);
		PdfPCell cell;
		Stream.of("id", "clientName", "productName", "productQuantity", "productPrice", "totalPrice")
				.forEach(columnTitle -> {
					PdfPCell header = new PdfPCell();
					header.setBackgroundColor(BaseColor.LIGHT_GRAY);
					header.setBorderWidth(2);
					header.setPhrase(new Phrase(columnTitle));
					table.addCell(header);
				});

		ArrayList<Bill> list = new ArrayList<Bill>();
		list = BillRepository.findAll();

		for (Bill i : list) {

			cell = new PdfPCell(new Phrase(i.getId().toString()));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(i.getClientName()));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(i.getProductName()));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(String.valueOf(i.getProductQuantity())));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(String.valueOf(i.getProductPrice())));
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(String.valueOf(i.getTotalPrice())));
			table.addCell(cell);

		}
		document.add(table);
		document.close();
	}

}