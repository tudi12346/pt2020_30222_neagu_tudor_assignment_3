package com.assignment3.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.assignment3.model.Order;

/**
 * 
 * Repository class used to connect and execute operations on the database
 * 
 * @author Tudor Neagu
 */
public class OrderRepository {
	/**
	 * Attributes are a Logger and the Query strings
	 */
	protected static final Logger LOGGER = Logger.getLogger(OrderRepository.class.getName());
	private static final String insert = "INSERT INTO warehouse.order (clientId,ProductId)" + " VALUES (?,?)";
	private static final String selectLast = "SELECT * FROM warehouse.order ORDER BY ID DESC LIMIT 1";

	/**
	 * Inserts order object into the database order table
	 * 
	 * @param order
	 * @return true if the order was inserted successfully; false otherwise
	 */
	public static boolean insert(Order order) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		try {
			insertStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

			insertStatement.setLong(1, order.getClientId());
			insertStatement.setLong(2, order.getProductId());

			insertStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product repo: insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		return false;
	}

	/**
	 * 
	 * @return Long id representing the order id of the last element introduced in
	 *         the order table
	 */
	public static Long getLastId() {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Long id = -1L;
		try {
			statement = connection.prepareStatement(selectLast, Statement.RETURN_GENERATED_KEYS);
			rs = statement.executeQuery();
			if (rs.next()) {
				id = rs.getLong("id");
			}

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product repo: insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return id;
	}
}
