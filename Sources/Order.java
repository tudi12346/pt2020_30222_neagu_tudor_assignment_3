package com.assignment3.model;
/** Represents an order
 *  */
public class Order {
	/** Attributes order
     *  id The order's id.
     *  clientId The client's id.
     *  ProductId The product's id.
     */
	private Long id;
    private Long clientId;
    private Long ProductId;
    /**
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
     * @param Long id contains order's id
     */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return clientId
	 */
	public Long getClientId() {
		return clientId;
	}
	/**
     * @param Long clientId contains client's id
     */
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return ProductId
	 */
	public Long getProductId() {
		return ProductId;
	}
	/**
     * @param Long productId contains product's id
     */
	public void setProductId(Long productId) {
		ProductId = productId;
	}
	

	

}