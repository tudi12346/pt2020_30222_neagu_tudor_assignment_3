package com.assignment3.model;

/** Represents a client
 */
public class Client {
	
	/** Attributes client
     *  id The client's id.
     *  name The client's name.
     *  address The client's address.
     */
	private Long id;
	private String name;
	private String address;

	/** Creates a client
     * @param name The client�s name.
     * @param address The client�s address.
     */
	public Client( String name, String address) {
		this.setName(name);
		this.setAddress(address);
	}
	/** Creates a client
     * @param id The client�s id.
     * @param name The client�s name.
     * @param address The client�s address.
     */
	public Client(Long id, String name, String address) {
		this.setId(id);
		this.setName(name);
		this.setAddress(address);
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
     * @param name string containing the name
     */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	 /**
     * @param address string containing the address
     */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
     * @param long id representing client id
     */
	public void setId(Long id) {
		this.id = id;
	}

}
