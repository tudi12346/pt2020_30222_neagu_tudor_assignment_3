package com.assignment3.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.assignment3.model.Bill;

/**
 * 
 * Repository class used to connect and execute operations on the database
 * 
 * @author Tudor Neagu
 */
public class BillRepository {
	/**
	 * Attributes are a Logger and the Query strings
	 */
	protected static final Logger LOGGER = Logger.getLogger(BillRepository.class.getName());
	private static final String insert = "INSERT INTO bill (id,clientName,productName,productQuantity,productPrice,totalPrice)"
			+ " VALUES (?,?,?,?,?,?)";
	private final static String findAll = "SELECT * FROM bill";

	/**
	 * Inserts bill object into the database bill table
	 * 
	 * @param bill
	 * @return true if the bill was inserted successfully; false otherwise
	 */
	public static boolean insert(Bill bill) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		try {
			insertStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setLong(1, bill.getId());
			insertStatement.setString(2, bill.getClientName());
			insertStatement.setString(3, bill.getProductName());
			insertStatement.setDouble(4, bill.getProductQuantity());
			insertStatement.setDouble(5, bill.getProductPrice());
			insertStatement.setDouble(6, bill.getTotalPrice());

			insertStatement.executeUpdate();
			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product repo: insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		return false;
	}

	/**
	 * Retrieves all the bills from the bill table
	 * 
	 * @return ArrayList<Bill> list of bill objects created form the existing bills
	 *         in the database's bill table
	 */
	public static ArrayList<Bill> findAll() throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		ArrayList<Bill> bills = new ArrayList<Bill>();

		try {
			statement = connection.prepareStatement(findAll);
			rs = statement.executeQuery();
			while (rs.next()) {
				Bill bill = new Bill();
				// "clientName", "productName", "productQuantity", "productPrice", "totalPrice"
				bill.setId(rs.getLong("id"));
				bill.setClientName(rs.getString("clientName"));
				bill.setProductName(rs.getString("productName"));
				bill.setProductQuantity(rs.getDouble("productQuantity"));
				bill.setProductPrice(rs.getDouble("productPrice"));
				bill.setTotalPrice();

				bills.add(bill);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "BillRepo: findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return bills;
	}
}
