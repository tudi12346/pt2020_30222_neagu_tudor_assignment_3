package com.assignment3.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Class for displaying the error report Contains only one static method to
 * create the report
 */
public class ErrorReport {
	/**
	 * Static method to create the report.
	 */
	public static void reportError(String clientName, String productNamee, Double quantity)
			throws FileNotFoundException, DocumentException, InterruptedException {
		Document document = new Document();

		String outputFile = "ReporterrorBill" + clientName + "_" + productNamee + "_" + quantity + ".pdf";
		outputFile = outputFile.replaceAll("[\\s\\[\\]/:]", "_");
		PdfWriter.getInstance(document, new FileOutputStream(outputFile));

		document.open();

		document.addTitle("Insufficient stock for order " + clientName + " " + productNamee + " " + quantity + ".");

		Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, BaseColor.MAGENTA);
		Chunk chunk = new Chunk(
				"Insufficient stock for order " + clientName + " " + productNamee + " " + quantity + ".", font);

		document.add(chunk);
		document.close();
		Thread.sleep(1000);
	}
}
